#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "mpi.h"
#include "../include/task.h"
#include "../include/platform.h"

Task *load_tasks(const char *, int *);
Platform *load_platform(const char *);
void print_solution(Task *, int, Platform *, double, double);
void get_solution(Task *, int, Platform *, Task *, Platform *, double *, double*, int, int, int);//, MPI_Datatype *, MPI_Datatype *, MPI_Datatype *);
void crear_tipo_datos_device(int , MPI_Datatype *);
void crear_tipo_datos_tasks(int , MPI_Datatype *);