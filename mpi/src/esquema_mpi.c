#include <mpi.h>
#include <math.h>
#include <float.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <stdbool.h>

#include "../include/task.h"
#include "../include/platform.h"
#include "../include/io.h"


Platform *sort_platform(Platform *platform, int rank)
{
    Platform *sorted_platform = (Platform *)malloc(sizeof(Platform));
    sorted_platform->n_devices = platform->n_devices;
    assert(sorted_platform);

    sorted_platform->devices = platform->devices;
    assert(sorted_platform->devices);

    Device temp;

    for (int i = 0; i < sorted_platform->n_devices - 1; i++)
    {
        for (int j = i + 1; j < sorted_platform->n_devices; j++)
        {
            if ((sorted_platform->devices[j].consumption / sorted_platform->devices[j].inst_core) <
                (sorted_platform->devices[i].consumption / sorted_platform->devices[i].inst_core))
            {
                temp = sorted_platform->devices[j];
                sorted_platform->devices[j] = sorted_platform->devices[i];
                sorted_platform->devices[i] = temp;
            }
        }
    }
    if(rank ==0)
        for (int i = 0; i < sorted_platform->n_devices; i++)
        {
            printf("%d\n", sorted_platform->devices[i].id);
        }

    return sorted_platform;
}


void generar(int nivel, int *s, int *executed)
{
    s[nivel]++;
    executed[s[nivel]] = 1;//la marcamos como tarea ejecutada
}

bool solucion(int nivel, int n_tasks, int *s, int *executed, Task *tasks){

    if (nivel < (n_tasks - 1))
    {
        return false;
    }

    for (int i = 0; i < nivel; i++)
    {
        if ((s[nivel] == s[i]))
            //Si el ultimo nodo ya exise en la solucion salir (nodo repetido)
            return false;
    }
    return true;
}

bool criterio(int nivel, int *s, int *executed, Task *tasks)
{
    for (int i = 0; i < nivel; i++)
    {
        if (s[nivel] == s[i])
        {
            //Si el nodo actual ya exise en la solucion no se cumple el criterio (repetido)
            return false;
        }
    }

    /**
     * comprueba si la solución parcial se puede convertir en una solución del problema, esto es que 
     * se hayan ejecutado las dependencias necesarias para ejecutar la tarea siguiente.
     * */
    int *deps = tasks[s[nivel]].dep_tasks;
    int n_deps = tasks[s[nivel]].n_deps;
    int i = 0;
    // for(int i = 0; i < n_deps;i++){
    //     int t = 0;
    //     while(t<=nivel && deps[i]!=s[t]){
    //         t++;
    //     }
    //     if(t>nivel)
    //         return false;
    // }
    // return true;
    while (i < n_deps && executed[deps[i]] == 1)
    {
        i++;
    }
    
    //si las tareas dependientes estan ejecutadas entonces se cumple el criterio
    if (i >= n_deps)
    {
        return true;
    }
    else
    {
        executed[s[nivel]] = 0;
        return false;
    }
}

bool masHermanos(int nivel, int n_tasks, int *s, int *executed)
{
    return s[nivel] < (n_tasks - 1);
}

void retroceder(int *nivel, int *s, int *executed)
{
    s[*nivel] = -1;
    *nivel -= 1;
    //Cuando retrocedemos un nivel cambiamos a la siguiente tarea, por eso la tarea previa existente la desmarcamos de ejecutadas 
    executed[s[*nivel]] = 0;
}

int getCoreLibre(int *cores_libres, Platform *sorted_platform)
{

    int icore = -1;

    for (int i = 0; i < sorted_platform->n_devices; i++)
    {

        if (cores_libres[i] > 0)
        {
            icore = i;
            return icore;
        }
    }

    return icore;
}

bool dependencia(Task *tasks, int t_act, int t_sig){
    for(int i=0; i<tasks[t_sig].n_deps; i++){
        if (tasks[t_sig].dep_tasks[i] == tasks[t_act].id)
        {
            return true;
        }
    }
    return false;
}

void copy_selected_dev( int n_tasks, Platform *to_selected_dev, Platform * from_selected_dev){
    for(int i=0; i<n_tasks; i++) {
            to_selected_dev[i].n_devices = from_selected_dev[i].n_devices;
            for(int j = 0; j < from_selected_dev[i].n_devices;j++){
                to_selected_dev[i].devices[j].hyperthreading = from_selected_dev[i].devices[j].hyperthreading;
                to_selected_dev[i].devices[j].consumption = from_selected_dev[i].devices[j].consumption;
                to_selected_dev[i].devices[j].id = from_selected_dev[i].devices[j].id;
                to_selected_dev[i].devices[j].inst_core = from_selected_dev[i].devices[j].inst_core;
                to_selected_dev[i].devices[j].n_cores=from_selected_dev[i].devices[j].n_cores;
            }
        }
}

void fill_selected_dev( Platform * selected_dev, int tarea, int sid, int id, int n_cores, int inst_core, int consumption, int ht){
    //Si no tiene dispositivo seeleccionado aumentamos el nº dispositivos
    if(selected_dev[tarea].devices[sid].id==-1)
        selected_dev[tarea].n_devices++; 
    selected_dev[tarea].devices[sid].id = id;
    selected_dev[tarea].devices[sid].n_cores = n_cores;
    selected_dev[tarea].devices[sid].consumption = consumption;
    selected_dev[tarea].devices[sid].inst_core = inst_core;
    selected_dev[tarea].devices[sid].hyperthreading = ht;
                
}


bool assign_devices(int *s, int n_tasks, Task *tasks, Platform *sorted_platform, Platform *selected_dev, Platform * selected_dev_aux, double *toa, double *coa)
{
	int n_devs = getNumDevices(sorted_platform);
	for(int i=0; i<n_tasks; i++) {
		selected_dev_aux[i].n_devices = 0;
		for(int j = 0; j < n_devs;j++){
			selected_dev_aux[i].devices[j].hyperthreading=0;
			selected_dev_aux[i].devices[j].consumption = 0.0;
			selected_dev_aux[i].devices[j].id = -1;
			selected_dev_aux[i].devices[j].inst_core = 0;
			selected_dev_aux[i].devices[j].n_cores=0;	
		}
	}
    double tiempo = 0;

    double energia = 0;

    const int CAPACIDAD_COMPUTACIONAL = getComputationalCapacity(sorted_platform);

    int c = CAPACIDAD_COMPUTACIONAL;

    //indica el indice de la tarea en la secuencia

    int tarea = 0;

    int inst_tarea_actual = tasks[s[tarea]].n_inst;

    // bool *asignadas = (bool *)malloc(n_tasks * sizeof(bool));

    // memset(asignadas, false, sizeof(bool) * n_tasks);

    int *cores_libres = (int *)malloc(sorted_platform->n_devices * sizeof(int));

    for (int i = 0; i < sorted_platform->n_devices; i++)
    {
        cores_libres[i] = getNumCores(sorted_platform->devices[i]);
    }

    while (tarea < n_tasks)
    {
            //considerar hiperthreading
            bool ht = false;
            int idev = 0;
            //  printf("tarea=%d inst: %d c:%d\n",s[tarea], inst_tarea_actual, c);
            //buscamos si se puede hacer hp con los cores restantes de cualquier dispositivo
            //asummo que el hiperthreading solo se puede hacer si todos los cores del dispositivo están disponibles
            //las intrucciones de la tarea actual totales o restantes deben ser igual al doble de las instrucciones que puede ejecutar el dispositivo con todos
            //los cores libres
            while(idev < sorted_platform->n_devices && ht == false){
                if ( (cores_libres[idev]==getNumCores(sorted_platform->devices[idev])) && (inst_tarea_actual == (2 * (cores_libres[idev] * getInstCore(sorted_platform->devices[idev]))))){
                    ht = true;
                }
                else idev ++;
            }
           
            if(ht == true){
                //restar c
                int inst_ejecutadas = cores_libres[idev] * getInstCore(sorted_platform->devices[idev]);
                tiempo += (double)inst_ejecutadas / (double)CAPACIDAD_COMPUTACIONAL;
                // printf("Tarea = %d; Tiempo ht = %.2lf\n", tiempo,tarea);

                energia += getConsumptionCore(sorted_platform->devices[idev]) * cores_libres[idev] * 1.1;
                int sid = getNumDevices(&selected_dev_aux[tarea]);
                fill_selected_dev(selected_dev_aux, tarea,sid,getDevId(sorted_platform->devices[idev]), cores_libres[idev],
                    getInstCore(sorted_platform->devices[idev]),getConsumptionCore(sorted_platform->devices[idev]), 1);
                c -= inst_ejecutadas;
                //se ejecutan todas las instrucciones
                inst_tarea_actual = 0;
                //se usan todos los cores libres
                cores_libres[idev] = 0;
                
            }
            else {
                while ((inst_tarea_actual > 0) && (getCoreLibre(cores_libres, sorted_platform) != -1))
                {
                    
                    idev = getCoreLibre(cores_libres, sorted_platform);
                    int n_cores = ceil((double)inst_tarea_actual / (double)getInstCore(sorted_platform->devices[idev]));
                    if (n_cores > cores_libres[idev])
                    {
                        //Es posible que las insttruciones actuales sean menor que las instrucciones de los cores a usar
                        int inst_ejecutadas = cores_libres[idev] * getInstCore(sorted_platform->devices[idev]);                        
                        tiempo += (double)inst_ejecutadas / (double)CAPACIDAD_COMPUTACIONAL;
                    //    printf("Tarea = %d; Tiempo = %.2lf\n", tiempo,tarea);
                        energia += getConsumptionCore(sorted_platform->devices[idev]) * cores_libres[idev];
                        int sid = getNumDevices(&selected_dev_aux[tarea]);
                        fill_selected_dev(selected_dev_aux, tarea,sid,getDevId(sorted_platform->devices[idev]),cores_libres[idev],
                            getInstCore(sorted_platform->devices[idev]),getConsumptionCore(sorted_platform->devices[idev]), 0);

                        inst_tarea_actual -= inst_ejecutadas;
                        //restar c
                        c -= inst_ejecutadas;
                        //restar cores_libres
                        cores_libres[idev] = 0;
                        //asignar_core
                    }

                    else
                    {
                        int inst_ejecutadas =  n_cores * getInstCore(sorted_platform->devices[idev]);
                        tiempo += ( (double)(inst_tarea_actual) / (double)CAPACIDAD_COMPUTACIONAL);
                        //  printf("Tarea = %d; Tiempo = %.2lf\n", tiempo,tarea);
                        energia += getConsumptionCore(sorted_platform->devices[idev]) * n_cores; 
                         int sid = getNumDevices(&selected_dev_aux[tarea]);   
                         fill_selected_dev(selected_dev_aux, tarea,sid,getDevId(sorted_platform->devices[idev]),n_cores,
                            getInstCore(sorted_platform->devices[idev]),getConsumptionCore(sorted_platform->devices[idev]), 1);
                        //restar c
                        if(inst_tarea_actual != (n_cores * getInstCore(sorted_platform->devices[idev]))){
                            // printf("\ninst_tarea_actual=%d %d\n",inst_tarea_actual, n_cores * getInstCore(sorted_platform->devices[idev]));
                        }
                        c -= inst_ejecutadas;
                        //actualizar el número de intrucciones
                        inst_tarea_actual = 0;
                        //restar cores_libres
                        cores_libres[idev] -= n_cores;
                        //asignar los que se pueda
                    }
                }
            }
            
            bool d = false;
            if((tarea < (n_tasks-1)) && (c > 0))
                d = dependencia(tasks, s[tarea],s[tarea+1]);
            
            if (c == 0 || d)
            {
                if(d){
                    tiempo += (double)c/(double)CAPACIDAD_COMPUTACIONAL;
                    // printf("Tarea = %d; Tiempo Dep = %.2lf\n", tiempo,tarea);
                }
               //poner los dispositivos libres
                for (int i = 0; i < sorted_platform->n_devices; i++)
                {
                    cores_libres[i] = sorted_platform->devices[i].n_cores;
                }
                c = CAPACIDAD_COMPUTACIONAL;
            }

            if (inst_tarea_actual == 0)

            {
                tarea++;
                if(tarea < n_tasks)
                    inst_tarea_actual = tasks[s[tarea]].n_inst;
            }
        }
    
    free(cores_libres);

    // printf("\nENERGIA %lf %lf TTIEMPO****** %lf\n", energia, *coa, tiempo);
    bool copy = false;
    if(tiempo < *toa)
        copy = true;
    else if((tiempo == *toa) && (energia < *coa))
        copy = true;

    if(copy){
        copy_selected_dev(n_tasks, selected_dev, selected_dev_aux);
        *coa = energia;
        *toa = tiempo;
        return true;
    }
    else{
        return false;
    }
        
}


void get_solution(Task *tasks, int n_tasks, Platform *platform, Task *sorted_tasks, Platform *selected_dev, double *t, double *e, int rank, int np, int n_devs)//, MPI_Datatype * platform_type,	MPI_Datatype * device_type, MPI_Datatype * task_type)
{
    int i, ok;
    int n_elem;
    int inicio, fin;
    double toa, coa;
    int isSolution = 0;
    toa = coa = DBL_MAX;

    int *s = (int *) malloc(sizeof(int)*n_tasks);
    int *soa = (int *) malloc(sizeof(int)*n_tasks);
    int *executed = (int *) malloc(sizeof(int)*n_tasks);

    // Inicializar Arrays
    memset(s, -1, sizeof(int)*n_tasks);
    memset(soa, -1, sizeof(int)*n_tasks);
    memset(executed, 0, sizeof(int)*n_tasks);
    
    // Ordenar dispositivos de menor a mayor consumo/instruccion
    Platform *sorted_platform = sort_platform(platform, rank);
	// int n_devs = getNumDevices(platform);

	MPI_Datatype device_type;
	crear_tipo_datos_device(n_devs, &device_type);

    Platform *selected_dev_aux = (Platform *) malloc(n_tasks*sizeof(Platform));
	assert(selected_dev_aux);
		
    for(int i=0; i<n_tasks; i++) {
        selected_dev_aux[i].devices = (Device *) malloc((2*n_devs)*sizeof(Device));
    }
	
    
    // Determinar reparto de hijos en nivel 0 entre procesos MPI
    if (rank == 0) {
        int n_elem = n_tasks / np;
    
        for(i = 1; i < np; i++) {
                inicio = ((n_tasks+0.0)/np) *i;
                fin = ((n_tasks+0.0)/np) *(i+1);
                MPI_Send(&inicio, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
                MPI_Send(&fin, 1, MPI_INT, i, 1, MPI_COMM_WORLD);  
        }
        inicio = 0;
        fin = ((n_tasks+0.0)/np) * 1;
		
    }
    else {
        MPI_Recv(&inicio, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		MPI_Recv(&fin, 1, MPI_INT, 0, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        // printf("nodo %d: inicio=%d,fin=%d\n", rank,inicio,fin);
    }
    // Paralelizacion con MPI
    for(i=inicio; i<fin; i++)
    {
        s[0] = i;
        //si la primera tarea es una dependencia, pasar directamente a la siguiente, nivel=0
        executed[i]=1;
		if(!criterio(0, s, executed, tasks)){continue;}
        //  printf("nodo %d:: tarea %d\n", rank, i);
        int nivel = 1;
        do {
            generar(nivel, s, executed);
            //printf("%d %d %d %d %d\n", s[0], s[1], s[2], s[3], s[4]);//, s[5], s[6], s[7], s[8], s[9]);
            //printf("%d %d %d %d %d\n", executed[0], executed[1], executed[2], executed[3], executed[4]);
            if (solucion(nivel, n_tasks, s, executed, tasks)) {
                // Obtener Asignacion de Dispositivos a Secuencia de Tareas
                ok = assign_devices(s, n_tasks, tasks, sorted_platform, selected_dev,selected_dev_aux, &toa, &coa);
                
                if (ok) { // Actualizar Solucion
                    // printf("%d %d %d %d %d\n", s[0], s[1], s[2], s[3], s[4]);//, s[5], s[6], s[7], s[8], s[9]);
                    memcpy(soa, s, sizeof(int)*n_tasks);
                    *t = toa;
                    *e = coa;
                    isSolution = 1;
                }
            }

            if (criterio(nivel, s, executed, tasks) && (nivel < n_tasks-1)) { nivel++; } 
            else {
                while (!masHermanos(nivel, n_tasks, s, executed) && (nivel > 0)) {
                    retroceder(&nivel, s, executed);
                }
            }
        } while(nivel > 0);
    }

    int tam_buffer = 8 + 8 + (n_tasks * 4) + (n_tasks * 4) * (24 * n_devs);
    void * buffer;
    if(isSolution || rank ==0)
        buffer = (void *) malloc(tam_buffer);
    int pos = 0;
    // El proceso 0 se queda con la mejor solucion de las obtenidas por cada proceso
    if (rank == 0) {
        double best_toa;
        double best_coa;
        // int *buff = (int *) malloc(sizeof(int)*n_tasks);
        for(i = 1; i < np; i++) {
            MPI_Recv(&isSolution, 1, MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            if(isSolution==0) continue;
            // printf("Soluciont %d nodo %d\n",isSolution, i);
            pos = 0;
            MPI_Recv(buffer,tam_buffer,MPI_PACKED, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            MPI_Unpack(buffer, tam_buffer, &pos, &best_toa, 1, MPI_DOUBLE, MPI_COMM_WORLD);
            MPI_Unpack(buffer, tam_buffer, &pos, &best_coa, 1, MPI_DOUBLE, MPI_COMM_WORLD);
           
            // Si los valores recibidos mejoran la solucion optima actual...		
            // Actualizar solucion optima
            if((best_toa < toa) || (best_toa == toa && best_coa < coa)){
                MPI_Unpack(buffer, tam_buffer, &pos, soa, n_tasks, MPI_INT, MPI_COMM_WORLD);
                // memcpy(soa, buff, sizeof(int)*n_tasks);

                for(int i = 0; i < n_tasks; i++){
                    int n_devs;
                    MPI_Unpack(buffer, tam_buffer, &pos, &n_devs, 1, MPI_INT, MPI_COMM_WORLD); //n_task * 4
                    selected_dev[i].n_devices = n_devs;
                    for(int j = 0; j < n_devs; j++){
                        MPI_Unpack(buffer, tam_buffer, &pos, &selected_dev[i].devices[j], 1,device_type, MPI_COMM_WORLD);//sorte_platform_ndevs * device_type
                    }
                }   
                toa = best_toa;
                coa = best_coa;
            }
            
        }
        
	    // free(buff);
    }
    else {
        MPI_Send(&isSolution, 1, MPI_INT,0,0,MPI_COMM_WORLD);
        if(isSolution) {

            MPI_Pack(&toa, 1, MPI_DOUBLE, buffer, tam_buffer, &pos, MPI_COMM_WORLD);//8
            MPI_Pack(&coa, 1, MPI_DOUBLE, buffer, tam_buffer, &pos, MPI_COMM_WORLD);//8
            MPI_Pack(soa, n_tasks, MPI_INT, buffer, tam_buffer, &pos, MPI_COMM_WORLD);//n_task * 4
            for(int i = 0; i < n_tasks; i++){
                int n_devs = selected_dev[i].n_devices;
                MPI_Pack(&n_devs, 1, MPI_INT, buffer, tam_buffer, &pos, MPI_COMM_WORLD); //n_task * 4
                for(int j = 0; j < n_devs; j++){
                    MPI_Pack(&selected_dev[i].devices[j], 1, device_type, buffer, tam_buffer, &pos, MPI_COMM_WORLD);//sorte_platform_ndevs * device_type
                }
            }
            MPI_Send(buffer,tam_buffer,MPI_PACKED, 0, 0, MPI_COMM_WORLD);
        }
    }
    if(isSolution || rank ==0)
        free(buffer);
	
    // Devolver Solucion
    if(rank == 0) {
        *t = toa;
        *e = coa;
        for(int i = 0; i < n_tasks; i++){
            sorted_tasks[i] = tasks[soa[i]];
        }
        // Imprimir mejor solucion
        //print(soa, t, e);
    }
    free(s);
    free(soa);
    free(executed);
    for(int i=0; i<n_tasks; i++) {
            free(selected_dev_aux[i].devices);
        }
    free(selected_dev_aux);
    free(sorted_platform);
}
