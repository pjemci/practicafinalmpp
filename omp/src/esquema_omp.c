#include <math.h>
#include <float.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <stdbool.h>
#include <omp.h>

#include "../include/platform.h"
#include "../include/task.h"

Platform *sort_platform(Platform *platform)
{
    Platform *sorted_platform = (Platform *)malloc(sizeof(Platform));
    sorted_platform->n_devices = platform->n_devices;
    assert(sorted_platform);

    sorted_platform->devices = platform->devices;
    assert(sorted_platform->devices);

    Device temp;

    for (int i = 0; i < sorted_platform->n_devices - 1; i++)
    {
        for (int j = i + 1; j < sorted_platform->n_devices; j++)
        {
            if ((sorted_platform->devices[j].consumption / sorted_platform->devices[j].inst_core) <
                (sorted_platform->devices[i].consumption / sorted_platform->devices[i].inst_core))
            {
                temp = sorted_platform->devices[j];
                sorted_platform->devices[j] = sorted_platform->devices[i];
                sorted_platform->devices[i] = temp;
            }
        }
    }

    for (int i = 0; i < sorted_platform->n_devices; i++)
    {
        printf("%d\n", sorted_platform->devices[i].id);
    }

    return sorted_platform;
}


void generar(int nivel, int *s, int *executed)
{
    s[nivel]++;
    executed[s[nivel]] = 1;//la marcamos como tarea ejecutada
}

bool solucion(int nivel, int n_tasks, int *s, int *executed, Task *tasks){

    if (nivel < (n_tasks - 1))
    {
        return false;
    }

    for (int i = 0; i < nivel; i++)
    {
        if ((s[nivel] == s[i]))
            //Si el ultimo nodo ya exise en la solucion salir (nodo repetido)
            return false;
    }
    return true;
}

bool criterio(int nivel, int n_tasks, int *s, int *executed, Task *tasks)
{
    for (int i = 0; i < nivel; i++)
    {
        if (s[nivel] == s[i])
        {
            //Si el nodo actual ya exise en la solucion no se cumple el criterio (repetido)
            return false;
        }
    }

    /**
     * comprueba si la solución parcial se puede convertir en una solución del problema, esto es que 
     * se hayan ejecutado las dependencias necesarias para ejecutar la tarea siguiente.
     * */
    int *deps = tasks[s[nivel]].dep_tasks;
    int n_deps = tasks[s[nivel]].n_deps;
    int i = 0;
    // for(int i = 0; i < n_deps;i++){
    //     int t = 0;
    //     while(t<=nivel && deps[i]!=s[t]){
    //         t++;
    //     }
    //     if(t>nivel)
    //         return false;
    // }
    // return true;
    while (i < n_deps && executed[deps[i]] == 1)
    {
        i++;
    }
    
    //si las tareas dependientes estan ejecutadas entonces se cumple el criterio
    if (i >= n_deps)
    {
        return true;
    }
    else
    {
        executed[s[nivel]] = 0;
        return false;
    }
}

bool masHermanos(int nivel, int n_tasks, int *s, int *executed)
{
    return s[nivel] < (n_tasks - 1);
}

void retroceder(int *nivel, int *s, int *executed)
{
    s[*nivel] = -1;
    *nivel -= 1;
    //Cuando retrocedemos un nivel cambiamos a la siguiente tarea, por eso la tarea previa existente la desmarcamos de ejecutadas 
    executed[s[*nivel]] = 0;
}

int getCoreLibre(int *cores_libres, Platform *sorted_platform)
{

    int icore = -1;

    for (int i = 0; i < sorted_platform->n_devices; i++)
    {

        if (cores_libres[i] > 0)
        {
            icore = i;
            return icore;
        }
    }

    return icore;
}

bool dependencia(Task *tasks, int t_act, int t_sig){
    for(int i=0; i<tasks[t_sig].n_deps; i++){
        if (tasks[t_sig].dep_tasks[i] == tasks[t_act].id)
        {
            return true;
        }
    }
    return false;
}

void copy_selected_dev( int n_tasks, Platform *to_selected_dev, Platform * from_selected_dev){
    for(int i=0; i<n_tasks; i++) {
            to_selected_dev[i].n_devices = from_selected_dev[i].n_devices;
            for(int j = 0; j < from_selected_dev[i].n_devices;j++){
                to_selected_dev[i].devices[j].hyperthreading = from_selected_dev[i].devices[j].hyperthreading;
                to_selected_dev[i].devices[j].consumption = from_selected_dev[i].devices[j].consumption;
                to_selected_dev[i].devices[j].id = from_selected_dev[i].devices[j].id;
                to_selected_dev[i].devices[j].inst_core = from_selected_dev[i].devices[j].inst_core;
                to_selected_dev[i].devices[j].n_cores=from_selected_dev[i].devices[j].n_cores;
            }
        }
}

void fill_selected_dev( Platform * selected_dev, int tarea, int sid, int id, int n_cores, int inst_core, int consumption, int ht){
    //Si no tiene dispositivo seeleccionado aumentamos el nº dispositivos
    if(selected_dev[tarea].devices[sid].id==-1)
        selected_dev[tarea].n_devices++; 
    selected_dev[tarea].devices[sid].id = id;
    selected_dev[tarea].devices[sid].n_cores = n_cores;
    selected_dev[tarea].devices[sid].consumption = consumption;
    selected_dev[tarea].devices[sid].inst_core = inst_core;
    selected_dev[tarea].devices[sid].hyperthreading = ht;
                
}


bool assign_devices(int *s, int n_tasks, Task *tasks, Platform *sorted_platform, Platform *selected_dev, Platform * selected_dev_aux, double *toa, double *coa)
{
	int n_devs = getNumDevices(sorted_platform);
	for(int i=0; i<n_tasks; i++) {
		selected_dev_aux[i].n_devices = 0;
		for(int j = 0; j < n_devs;j++){
			selected_dev_aux[i].devices[j].hyperthreading=0;
			selected_dev_aux[i].devices[j].consumption = 0.0;
			selected_dev_aux[i].devices[j].id = -1;
			selected_dev_aux[i].devices[j].inst_core = 0;
			selected_dev_aux[i].devices[j].n_cores=0;	
		}
	}
    double tiempo = 0;

    double energia = 0;

    const int CAPACIDAD_COMPUTACIONAL = getComputationalCapacity(sorted_platform);

    int c = CAPACIDAD_COMPUTACIONAL;

    //indica el indice de la tarea en la secuencia

    int tarea = 0;

    int inst_tarea_actual = tasks[s[tarea]].n_inst;

    // bool *asignadas = (bool *)malloc(n_tasks * sizeof(bool));

    // memset(asignadas, false, sizeof(bool) * n_tasks);

    int *cores_libres = (int *)malloc(sorted_platform->n_devices * sizeof(int));

    for (int i = 0; i < sorted_platform->n_devices; i++)
    {
        cores_libres[i] = getNumCores(sorted_platform->devices[i]);
    }

    while (tarea < n_tasks)
    {
            //considerar hiperthreading
            bool ht = false;
            int idev = 0;
            //  printf("tarea=%d inst: %d c:%d\n",s[tarea], inst_tarea_actual, c);
            //buscamos si se puede hacer hp con los cores restantes de cualquier dispositivo
            //asummo que el hiperthreading solo se puede hacer si todos los cores del dispositivo están disponibles
            //las intrucciones de la tarea actual totales o restantes deben ser igual al doble de las instrucciones que puede ejecutar el dispositivo con todos
            //los cores libres
            while(idev < sorted_platform->n_devices && ht == false){
                if ( (cores_libres[idev]==getNumCores(sorted_platform->devices[idev])) && (inst_tarea_actual == (2 * (cores_libres[idev] * getInstCore(sorted_platform->devices[idev]))))){
                    ht = true;
                }
                else idev ++;
            }
           
            if(ht == true){
                //restar c
                int inst_ejecutadas = cores_libres[idev] * getInstCore(sorted_platform->devices[idev]);
                tiempo += (double)inst_ejecutadas / (double)CAPACIDAD_COMPUTACIONAL;
                // printf("Tarea = %d; Tiempo ht = %.2lf\n", tiempo,tarea);

                energia += getConsumptionCore(sorted_platform->devices[idev]) * cores_libres[idev] * 1.1;
                int sid = getNumDevices(&selected_dev_aux[tarea]);
                fill_selected_dev(selected_dev_aux, tarea,sid,getDevId(sorted_platform->devices[idev]), cores_libres[idev],
                    getInstCore(sorted_platform->devices[idev]),getConsumptionCore(sorted_platform->devices[idev]), 1);
                c -= inst_ejecutadas;
                //se ejecutan todas las instrucciones
                inst_tarea_actual = 0;
                //se usan todos los cores libres
                cores_libres[idev] = 0;
                
            }
            else {
                while ((inst_tarea_actual > 0) && (getCoreLibre(cores_libres, sorted_platform) != -1))
                {
                    
                    idev = getCoreLibre(cores_libres, sorted_platform);
                    int n_cores = ceil((double)inst_tarea_actual / (double)getInstCore(sorted_platform->devices[idev]));
                    if (n_cores > cores_libres[idev])
                    {
                        //Es posible que las insttruciones actuales sean menor que las instrucciones de los cores a usar
                        int inst_ejecutadas = cores_libres[idev] * getInstCore(sorted_platform->devices[idev]);                        
                        tiempo += (double)inst_ejecutadas / (double)CAPACIDAD_COMPUTACIONAL;
                    //    printf("Tarea = %d; Tiempo = %.2lf\n", tiempo,tarea);
                        energia += getConsumptionCore(sorted_platform->devices[idev]) * cores_libres[idev];
                        int sid = getNumDevices(&selected_dev_aux[tarea]);
                        fill_selected_dev(selected_dev_aux, tarea,sid,getDevId(sorted_platform->devices[idev]),cores_libres[idev],
                            getInstCore(sorted_platform->devices[idev]),getConsumptionCore(sorted_platform->devices[idev]), 0);

                        inst_tarea_actual -= inst_ejecutadas;
                        //restar c
                        c -= inst_ejecutadas;
                        //restar cores_libres
                        cores_libres[idev] = 0;
                        //asignar_core
                    }

                    else
                    {
                        int inst_ejecutadas =  n_cores * getInstCore(sorted_platform->devices[idev]);
                        tiempo += ( (double)(inst_tarea_actual) / (double)CAPACIDAD_COMPUTACIONAL);
                        //  printf("Tarea = %d; Tiempo = %.2lf\n", tiempo,tarea);
                        energia += getConsumptionCore(sorted_platform->devices[idev]) * n_cores; 
                         int sid = getNumDevices(&selected_dev_aux[tarea]);   
                         fill_selected_dev(selected_dev_aux, tarea,sid,getDevId(sorted_platform->devices[idev]),n_cores,
                            getInstCore(sorted_platform->devices[idev]),getConsumptionCore(sorted_platform->devices[idev]), 1);
                        //restar c
                        if(inst_tarea_actual != (n_cores * getInstCore(sorted_platform->devices[idev]))){
                            // printf("\ninst_tarea_actual=%d %d\n",inst_tarea_actual, n_cores * getInstCore(sorted_platform->devices[idev]));
                        }
                        c -= inst_ejecutadas;
                        //actualizar el número de intrucciones
                        inst_tarea_actual = 0;
                        //restar cores_libres
                        cores_libres[idev] -= n_cores;
                        //asignar los que se pueda
                    }
                }
            }
            
            bool d = false;
            if((tarea < (n_tasks-1)) && (c > 0))
                d = dependencia(tasks, s[tarea],s[tarea+1]);
            
            if (c == 0 || d)
            {
                if(d){
                    tiempo += (double)c/(double)CAPACIDAD_COMPUTACIONAL;
                    // printf("Tarea = %d; Tiempo Dep = %.2lf\n", tiempo,tarea);
                }
               //poner los dispositivos libres
                for (int i = 0; i < sorted_platform->n_devices; i++)
                {
                    cores_libres[i] = sorted_platform->devices[i].n_cores;
                }
                c = CAPACIDAD_COMPUTACIONAL;
            }

            if (inst_tarea_actual == 0)

            {
                tarea++;
                if(tarea < n_tasks)
                    inst_tarea_actual = tasks[s[tarea]].n_inst;
            }
        }
    
    free(cores_libres);

    // printf("\nENERGIA %lf %lf TTIEMPO****** %lf\n", energia, *coa, tiempo);
    bool copy = false;
    if(tiempo < *toa)
        copy = true;
    else if((tiempo == *toa) && (energia < *coa))
        copy = true;

    if(copy){
        copy_selected_dev(n_tasks, selected_dev, selected_dev_aux);
        *coa = energia;
        *toa = tiempo;
        return true;
    }
    else{
        return false;
    }
        
}


void get_solution(Task *tasks, int n_tasks, Platform *platform, Task *sorted_tasks, Platform **selected_dev, double *t, double *e)
{
    int i, ok;
    int nivel, hilo;
    double toa, coa;
    toa = coa = DBL_MAX;

    int n_devs = getNumDevices(platform);
    // Obtener Numero de Hilos OpenMP
    int num_hilos = atoi(getenv("OMP_NUM_THREADS"));

    int *s[num_hilos];
    int *soa[num_hilos];
    int *executed[num_hilos];
    
    Platform *selected_dev_aux[num_hilos]; 
	for(int i=0; i<num_hilos; i++) {
		selected_dev_aux[i] = (Platform *) malloc(n_tasks*sizeof(Platform));
		assert(selected_dev_aux);
		
		for(int j=0; j<n_tasks; j++) {
			selected_dev_aux[i][j].devices = (Device *) malloc((2*n_devs)*sizeof(Device));
		}
	}
	
    // Inicializar Arrays
    for(i=0; i<num_hilos; i++) {
        s[i] = (int *) malloc(n_tasks*sizeof(int));
        memset(s[i], -1, sizeof(int)*n_tasks);
            
        soa[i] = (int *) malloc(n_tasks*sizeof(int));
        memset(soa[i], -1, sizeof(int)*n_tasks);
        
        executed[i] = (int *) malloc(n_tasks*sizeof(int));
        memset(executed[i], 0, sizeof(int)*n_tasks);
    }
    
    // Ordenar dispositivos de menor a mayor consumo/instruccion
    Platform *sorted_platform = sort_platform(platform);
    // Paralelizacion con OpenMP
    #pragma omp parallel for private(i, nivel, hilo) firstprivate(toa, coa, t, e) shared(selected_dev, selected_dev_aux) schedule(guided, 1)
    for(i=0; i<n_tasks; i++)
    {
        hilo = omp_get_thread_num();
        s[hilo][0] = i;
        //si la primera tarea es una dependencia, pasar directamente a la siguiente, nivel=0
        executed[hilo][i]=1;
        // printf("hilo %d:: tarea %d\n", hilo, i);
		if(!criterio(0, n_tasks, s[hilo], executed[hilo], tasks)){continue;}
    	nivel = 1;
	do {
	        generar(nivel, s[hilo], executed[hilo]);
            // printf("%d %d %d %d %d ,hilo %d\n", s[hilo][0], s[hilo][1], s[hilo][2],s[hilo][3], s[hilo][4], hilo);
	        if (solucion(nivel, n_tasks, s[hilo], executed[hilo], tasks)) {
                // printf("\nSOL: %d %d %d %d %d ,hilo %d\n", s[hilo][0], s[hilo][1], s[hilo][2],s[hilo][3], s[hilo][4], hilo);
                    // Obtener Asignacion de Dispositivos a Secuencia de Tareas
                ok = assign_devices(s[hilo], n_tasks, tasks, sorted_platform,selected_dev[hilo], selected_dev_aux[hilo], &toa, &coa);
            
                if (ok) { // Actualizar Solucion
                    memcpy(soa[hilo], s[hilo], sizeof(int)*n_tasks);
                    t[hilo] = toa;
                    e[hilo] = coa;
                }
            }
		
	        if ((nivel < n_tasks-1) && criterio(nivel, n_tasks, s[hilo], executed[hilo], tasks) ) { nivel++; } 
        	else {
	            while ((nivel > 0) && !masHermanos(nivel, n_tasks, s[hilo], executed[hilo])) {
	                retroceder(&nivel, s[hilo], executed[hilo]);
	            }
        	}
	} while(nivel > 0);
    }

    // El master se queda con la mejor solucion de las obtenidas por cada hilo
    int *best_sol, best_selected = 0;// = (int *) malloc(sizeof(int)*n_tasks);
    best_sol = soa[0];
    for(i=1; i<num_hilos; i++) {
	    if((t[i] < t[0]) ||(t[i] == t[0] && e[i] < e[0])){
            t[0] = t[i];
            e[0] = e[i];
            // printf("soa= %d hilo %d tiempo %f e %f\n", soa[i][0], i, t[i], e[i]);
            best_sol = soa[i];
            best_selected = i;
        }
    }
    if(best_selected != 0){
        Platform * aux = selected_dev[0];
        selected_dev[0] = selected_dev[best_selected];
        selected_dev[best_selected] = aux;
    }
     for(int i = 0; i < n_tasks; i++){
            // printf("BEST SOL= %d", best_sol[i]);
            sorted_tasks[i] = tasks[best_sol[i]];

    }

  
    //  print_solution(sorted_tasks, n_tasks, selected_dev[0], t[0], e[0]);

    // Guardar Solucion
    // *t = *toa;  *e = *coa;

    // Imprimir mejor solucion
    // print(best_sol, t, e);

    // Liberar Memoria
    for(i=0; i<num_hilos; i++) {
        free(s[i]);
        free(soa[i]);
        free(executed[i]);
    }
    for(int i=0; i<num_hilos; i++) {
		for(int j=0; j<n_tasks; j++) {
			free(selected_dev_aux[i][j].devices);
		}
		free(selected_dev_aux[i]);
	}
    free(sorted_platform);
	

}
